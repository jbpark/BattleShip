import java.util.*;

class Board{
	public enum State{EMPTY ("[ ]"), SHIP_SELECTED ("[ ]"), SHIP_SHOT ("[O]"), MISSED_SHOT ("[X]");
		
		private String state;
		
		State(String state){
			this.state = state;
		}
		
		public String get_state(){
			return this.state;
		}
	}
	
	private State[][] board;
	private final int row = 5;
	private final int col = 5;
	
	
	public Board(){
		board = new State[row][col];
		for(int i=0; i<row; i++){
			for(int j=0; j<col; j++){
				board[i][j] = State.EMPTY;
			}
		}
	}
	
	public void change_State(int row, int col, State state){
		board[row][col] = state;
	}
	
	public State get_State(int row, int col){
		return board[row][col];
	}
	
	public void display_Board(){
		for(int i=0; i<row; i++){
			for(int j=0; j<col; j++){
				System.out.print(board[i][j].get_state() + " ");
			}
			System.out.print("\n");
		}
	}
}

class Play_BShip{
	
	private final int row = 5;
	private final int col = 5;
	private final int num_ships = 10;
	private int num_shot = 0;
	private Board board;
	Scanner in = new Scanner(System.in);
	
	
	public void play(){
		board = new Board();
		init();
		board.display_Board();
		
		do{
			try{
				System.out.println("Please enter a coordinate you would like to shoot in the format row,column (e.g. 4,4)");
				String[] coordinate = in.next().split(",");
				int row = Integer.parseInt(coordinate[0]);
				int col = Integer.parseInt(coordinate[1]);
				if(board.get_State(row,col) == Board.State.SHIP_SELECTED){
					board.change_State(row,col,Board.State.SHIP_SHOT);
					System.out.println("You have shot a ship!");
					num_shot++;
				} else if(board.get_State(row,col) == Board.State.EMPTY){
					board.change_State(row,col,Board.State.MISSED_SHOT);
					System.out.println("You have missed!");
				}else{
					System.out.println("You have already shot that coordinate!");
				}
				board.display_Board();
			}catch(Exception e1){
				System.out.print("ERROR! ");
			}
				
		}while(num_shot<10);
	}
	
	public void init(){
		Random r = new Random();
		for(int i=0; i<num_ships; i++){
			int row = r.nextInt(this.row);
			int col = r.nextInt(this.col);
			board.change_State(row,col,Board.State.SHIP_SELECTED);
		}
	}
}

public class BShip{
	public static void main(String[] args){
		Play_BShip game = new Play_BShip();
		game.play();
	}
}